import { Component } from '@angular/core';
import {
  addScopeListener,
  addTokenListener,
  addWbsUrlListener,
  Scope,
  Token,
  WbsUrl
} from '@empaia/vendor-app-communication-interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'vaci-test';
}

function mySampleFunction() {
  addScopeListener((scope: Scope) => {
    // the scope object contains the attribute id which is the scopeId
    const scopeId = scope.id;
    // store the scope id in your app
  });

  addTokenListener((token: Token) => {
    // the token object contains the attribute value which is the access token
    const accessToken = token.value;
    // store the access token in your app
  });

  addWbsUrlListener((wbsUrl: WbsUrl) => {
    // the wbsUrl object contains the attribute url which is the base url to the Workbench Service 2.0
    const url = wbsUrl.url;
    // store the Workbench Service 2.0 URL in your app for the base URL for further api calls
  });
}